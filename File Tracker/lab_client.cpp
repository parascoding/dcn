

#include <iostream>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include<bits/stdc++.h>
#include <fstream>
using namespace std;
//Client side
int main()
{
    
    string temp;
    cout << "Enter demanded file name\n";
    cin >> temp;
    int n = temp.length();
    char requestedFile[n];
    for(int i=0;i<n;i++)
        requestedFile[i] = temp[i];
    vector<string> ipVec = {"172.16.16.138", "172.16.16.139", "172.16.16.140", "172.16.22.209"};
    for(string &s : ipVec){

        const char *serverIp = s.c_str(); 
        int port = 15004; 
        //create a message buffer 
        char msg[1500]; 
        //setup a socket and connection tools 
        struct hostent* host = gethostbyname(serverIp); 
        sockaddr_in sendSockAddr;   
        bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
        sendSockAddr.sin_family = AF_INET; 
        sendSockAddr.sin_addr.s_addr = 
            inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
        sendSockAddr.sin_port = htons(port);
        int clientSd = socket(AF_INET, SOCK_STREAM, 0);
        //try to connect...
        int status = connect(clientSd,
                            (sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
        if(status < 0)
        {
            // cout<<"Error connecting to socket!"<<endl;
            continue;
        }
        
        send(clientSd, (char*)&requestedFile, strlen(requestedFile), 0);
        recv(clientSd, (char*)&msg, sizeof(msg), 0);
        if(strcmp("YES", msg)==0){
            cout << "Downloading file from "<<s<<endl;
            close(clientSd);
            return 0;
        } 
        close(clientSd);
    }

   
    // while(1)
    // {
    //     string data;
    //     // getline(cin, data);
    //     memset(&msg, 0, sizeof(msg));//clear the buffer
    //     recv(clientSd, (char*)&msg, sizeof(msg), 0);
    //     cout << msg << endl;
    //     memset(&msg, 0, sizeof(msg));//clear the buffer

        
    //     cout << ">";
    //     getline(cin, data);
    //     strcpy(msg, data.c_str());
    //     send(clientSd, (char*)&msg, strlen(msg), 0);
    //     // break;
        
    //     // bytesWritten += send(clientSd, (char*)&msg, strlen(msg), 0);
    //     // cout << "Awaiting server response..." << endl;
    //     memset(&msg, 0, sizeof(msg));//clear the buffer
    //     // bytesRead += recv(clientSd, (char*)&msg, sizeof(msg), 0);
    //     if(!strcmp(msg, "exit"))
    //     {
    //         cout << "Server has quit the session" << endl;
    //         break;
    //     }
    //     {cout << msg << endl;}
    // }
    // close(clientSd);
    // cout << "********Session********" << endl;
    // cout << "Bytes written: " << bytesWritten << 
    // " Bytes read: " << bytesRead << endl;
    // cout << "Elapsed time: " << (end1.tv_sec- start1.tv_sec) 
    //   << " secs" << endl;
    cout << "Couldn't Donwload the file" << endl;
    return 0;    
}

