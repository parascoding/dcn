#include <bits/stdc++.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>

using namespace std;
int serverSd;
void fun(){
    sockaddr_in newSockAddr;
    socklen_t newSockAddrSize = sizeof(newSockAddr);
    int Sd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
}
int main(int argc, char *argv[]){
    if(argc != 2){
        cerr << "USAGE: IP";
        exit(0);
    }
    int port = stoi(argv[1]);
    sockaddr_in servAddr;

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(port);

    serverSd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverSd < 0){
        cout << "Error creating socket\n";
        exit(0);
    }
    int bindStatus = bind(serverSd, (struct sockaddr *)&servAddr, sizeof(servAddr));

    if(bindStatus < 0){
        cerr << "BIND FAILED";
        exit(0);
    }
    cout << "Waiting for some client to connect\n";
    listen(serverSd, 100);

    thread th1[100];
    for(int i = 0; i < 100; i++){
        th1[i] = thread(&fun);
    }

    for(int i = 0; i < 100; i++)
        th1[i].join();
    
    cout << "CLOSING\n";
    
}