#include <bits/stdc++.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

using namespace std;

sockaddr_in servAddr;
int port, serverSd, bindStatus;

class student{
public:
    int rollNo, marks;
    string name, city;
    student(int rno, int mrks, string nm, string cty){
        rollNo = rno;
        marks = mrks;
        name = nm;
        city = cty;
    }
};
vector<string> splitString(string s){
    stringstream ss(s);
    string temp;
    vector<string> v;
    while(getline(ss, temp, ' '))
        v.push_back(temp);
    return v;
}
void sendToClient(int Sd, string s){
    try{
        char temp[s.length() + 1];
        memset(&temp, 0, sizeof(temp));

        int ind = 0;
        for(int i = 0; i < s.length(); i++){
            temp[ind] = s[i];
            if(temp[ind] == '\0')
                ind--;
            ind++;
        }
        send(Sd, (char *)&temp, strlen(temp), 0);
    } catch(...){

    }
}
string receiveFromClient(int Sd){
    try{
        char temp[1000];
        memset(&temp, 0, sizeof(temp));
        recv(Sd, (char *)&temp, sizeof(temp), 0);
        string s(temp);
        return s;
    } catch(...){

    }
    return "-1";
}
void check(int Sd){
    fstream file;
    string s;
    file.open("users.txt");
    int last = -1;
    while(file){
        string temp;
        getline(file, temp);
        if(temp.length() == 0)
            break;
        vector<string> tempV = splitString(temp);
        int rollNo = stoi(tempV[0]);
        if(rollNo <= last){
            string resp = "File is not sorted based on roll numbers\n";
            sendToClient(Sd, resp);
            return;
        }
        last = rollNo;
    }
    string resp = "File is sorted based on roll numbers\n";
    sendToClient(Sd, resp);

} 
bool mycomp1(student s1, student s2){
    return s1.marks > s2.marks;
}
void findSortedOrderBasedOnMarks(int Sd){
    cout << "AT 88" << endl << flush;
    string ans = "";
    vector<student> students;
    fstream file;
    file.open("users.txt");
    while(file){
        string temp;
        getline(file, temp);
        if(temp.length() == 0)
            break;
        vector<string> tempVec = splitString(temp);
        
        int rollNo = stoi(tempVec[0]);
        string name = tempVec[1];
        int marks = stoi(tempVec[2]);
        string city = tempVec[3];
        
        student stud(rollNo, marks, name, city);
        students.push_back(stud);
    }
    sort(students.begin(), students.end(), mycomp1);
    for(int i = 1; i < students.size(); i++){
        ans += students[i].rollNo;
        ans += " ";
        ans += students[i].name;
        ans += " ";
        ans += students[i].marks;
        ans += " ";
        ans += students[i].city;
        ans += "\n";
    }
    sendToClient(Sd, ans);
    cout << ans << endl << flush;
}
void errorMessage(int Sd){
    cout << "ERROR" << endl << flush;
}
void fun(){
    try{    
        sockaddr_in newSockAddr;
        socklen_t newSockAddrSize = sizeof(newSockAddr);

        int Sd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);

        if(Sd < 0){
            cerr << "ERROR CONNECTING CLIENT\n";
        } else{
            char clientIP[100];
            inet_ntop( AF_INET, &(newSockAddr.sin_addr), clientIP, 100);
            cout << clientIP << " CONNECTED" << endl;
        }

        while(1){
            string s = "Enter\n1. For Checking if file is sorted according to roll number\n2. For getting students details sorted according to their marks\n";
            sendToClient(Sd, s);

            string resp = receiveFromClient(Sd);
            if(resp.length() == 0)
                break;
            int choice = -1;
            try{
                choice = stoi(resp);
            } catch(...){
                errorMessage(Sd);
                continue;
            }
            if(choice == 1){
                check(Sd);
            } else if(choice == 2){
                findSortedOrderBasedOnMarks(Sd);
            }
        }
    } catch(...){

    }
}
int main(int argc, char *argv[]){
    
    if(argc != 2){
        cerr << "Usage: Port" << endl;
        exit(0);
    }
    port = atoi(argv[1]);
    bzero((char *)&servAddr, sizeof(servAddr));

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(port);

    serverSd = socket(AF_INET, SOCK_STREAM, 0);

    if(serverSd < 0){
        cerr << "Can't create socket sucessfully" << endl;
        exit(0);
    }

    bindStatus = bind(serverSd, (struct sockaddr*)&servAddr, sizeof(servAddr));

    if(bindStatus < 0){
        cerr << "Error binding the socket\n";
        exit(0);
    }
    cout << "Waiting for some client to connect\n";

    listen(serverSd, 100);

    thread th1[100];
    int ind = 0;

    while(ind < 100)
        th1[ind++] = thread(&fun);
    ind = 0;
    while(ind < 100)
        th1[ind++].join();
    cout << "CLOSING" << endl;

    return 0;
}