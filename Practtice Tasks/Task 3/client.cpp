#include <bits/stdc++.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

using namespace std;

void sendToServer(int Sd, string s){
    char temp[s.length() + 1];
    memset(&temp, 0, sizeof(temp));

    int ind = 0;
    for(int i = 0; i < s.length(); i++){
        temp[ind] = s[i];
        if(temp[ind] == '\0')
            ind--;
        ind++;
    }
    send(Sd, (char *)&temp, sizeof(temp), 0);
}
string receiveFromServer(int Sd){
    char temp[1000];
    memset(&temp, 0, sizeof(temp));
    recv(Sd, (char *)&temp, sizeof(temp), 0);
    string s(temp);
    return temp;
}
void fun(int Sd){
    while(1){
        string s = receiveFromServer(Sd);
        cout << s << endl << flush;
        string resp;
        cin >> resp;
        sendToServer(Sd, resp);
        resp = receiveFromServer(Sd);
        cout << resp << endl << flush;
    }   
}
int main(int argc, char *argv[]){
    if(argc != 3){
        cerr << "Usage: ip_addr port" << endl;
        exit(0);
    }

    char *serverIp = argv[1];
    int port = atoi(argv[2]);

    sockaddr_in servAddr;
    bzero((char *)&servAddr, sizeof(servAddr));

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servAddr.sin_port = htons(port);

    int Sd = socket(AF_INET, SOCK_STREAM, 0);

    int status = connect(Sd, (sockaddr*)&servAddr, sizeof(servAddr));

    if(status < 0){
        cout << "Error connecting to socket\n";
        return -1;
    }
    cout << "Connected to server\n";

    fun(Sd);

    close(Sd);
    
    return 0;

}